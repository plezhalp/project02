﻿// Reptilian - GAME 120 and 121 Spring 2016
// SingletonButtonLoadLevel
// Script by Llane
// This script is attached to the canvas which has the scene load buttons on it
// The various levels are loaded via the buttons
// The level names are input and attached to each button in the Inspector

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SingletonButtonLoadLevel : MonoBehaviour {

	// Set up singleton variable
	public static SingletonButtonLoadLevel Instance {get; private set;}

	void Awake ()
	{
		// Check for other instances of me and if they exist, destroy me
		if (Instance != null  && Instance != this)
		{
			Destroy (gameObject);
		}

		// If no other, make me the instance and don't destroy me between scenes
		Instance = this;
		DontDestroyOnLoad(gameObject);
	}

	public void _LoadLevel(string levelName)
	{
		SceneManager.LoadScene(levelName);
		SingletonAudioSource.Instance.PlayEffect(1);           // Add this Audio Source Singleton for the click effect
	}	

	public void _LoadSavedGame()
	{
		SingletonGameController.Instance.Load();
		SingletonAudioSource.Instance.PlayEffect(1);           // Add this Audio Source Singleton for the click effect

	}


	public void _Quit()
	{
		SingletonAudioSource.Instance.PlayEffect(1);           // Add this Audio Source Singleton for the click effect
		Application.Quit ();
	}

}
