﻿using UnityEngine;
using System.Collections;

public class HealthPickup : MonoBehaviour {

	LevelManager accessLevelManager;

	// Use this for initialization
	void Start () 

	{
		// Set up easy access to the Level Manager script
		accessLevelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
	
	}
	
	// Update is called once per frame
	void Update () {
	

	}

	void OnTriggerEnter(Collider other)
	{
		accessLevelManager.AddHealth();
		Destroy(this.gameObject);
	}
}
