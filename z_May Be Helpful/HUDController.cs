﻿// Reptilian
// JCCC Spring 2016
// HUDController script is a singleton placed on the Canvas of the scene that first uses the HUD, the 10_HubWorld
// This script is used to update the HUD information about the Player
// Public variables are associated from the Canvas

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HUDController : MonoBehaviour 
{

	[Header ("HUD information")]
	public Image livesImage;
	public Image healthBarImage;
	public Image keysPlaced;
	public Text livesCountText;
	public Text villagersCountText;
	public Text villagersAvailableText;


	public static HUDController Instance;
	//public static HUDController Instance {get; private set;}


	// Use this for initialization
	void Start ()
	{
		Instance = this;

		DisableHUD();


		// Set up HUD initially

		//livesImage.sprite = Resources.Load<Sprite>("Sprites/lives_Sprite");
		//healthBarImage.sprite = Resources.Load<Sprite>("Sprites/playerHealth3_Sprite");
		//keysPlaced.sprite = Resources.Load<Sprite>("Sprites/keysPlaced0_Sprite");
		//villagersCountText.text = "0";
		//villagersAvailableText.text = "/" + "0";
		//livesCountText.text = "5";

		//GameObject.Find("LivesSprite").GetComponent <Image> ().sprite = livesImage.sprite;

		//GameObject.Find("LivesCount").GetComponent <Text> ().text = livesCountText.text;
		//GameObject.Find("HealthBarSprite").GetComponent <Image> ().sprite = healthBarImage.sprite;
		//GameObject.Find("KeysPlaced").GetComponent <Image> ().sprite = keysPlaced.sprite;
		//GameObject.Find("VillagersCount").GetComponent <Text> ().text = villagersCountText.text;
		//GameObject.Find("VillagersAvailable").GetComponent <Text> ().text = villagersAvailableText.text;

	}

	public void DisableHUD ()
	{
		// Get the current scene
		Scene currentScene = SceneManager.GetActiveScene();
		Debug.Log (" Current scene is " + currentScene.name);


		// Disable all HUD images for all scenes that don't use it

		if (currentScene.name == "OO_StartScene" || currentScene.name == "01_Splash_Scene" || 
			currentScene.name == "02_Credits" || currentScene.name == "03_MainMenu" || 
			currentScene.name == "04_Instructions" || currentScene.name == "WinScene")
		{
			livesImage.enabled = false;
			healthBarImage.enabled = false;
			keysPlaced.enabled = false;
			livesCountText.enabled = false;
			villagersCountText.enabled = false;
		}
	}

//	void Awake()
//	{
//		
//
//		if (Instance != null && Instance != this)
//		{
//			Destroy (gameObject);
//		}
//
//
//		// If no other, make me the instance and don't destroy me between scenes
//
//		Instance = this;
//
//		DontDestroyOnLoad (gameObject);
//	}

	void Update () 
	{
		
	
	}

	// Disable HUD for initial scenes


		 
	public void UpdateHUD (int playerLives, int playerHealth, int keysDropped, int villagerCount, int villagersAvailable) 
	//public void UpdateHUD () 
	{
		GameObject.Find("LivesCount").GetComponent <Text> ().text = playerLives.ToString();
		GameObject.Find("VillagersCount").GetComponent <Text> ().text = villagerCount.ToString();
		GameObject.Find("VillagersAvailable").GetComponent <Text> ().text = "/" + villagersAvailable.ToString();
		//Debug.Log("From UpdateHUD: Villagers Available are " + villagersAvailable.ToString() );

		switch (playerHealth)
		{
		case 3:
			GameObject.Find("HealthBarSprite").GetComponent <Image> ().sprite = Resources.Load<Sprite>("Sprites/playerHealth3_Sprite");
			break;
		case 2:
			GameObject.Find("HealthBarSprite").GetComponent <Image> ().sprite = Resources.Load<Sprite>("Sprites/playerHealth2_Sprite");
			break;
		case 1:
			GameObject.Find("HealthBarSprite").GetComponent <Image> ().sprite = Resources.Load<Sprite>("Sprites/playerHealth1_Sprite");
			break;
		case 0:
			GameObject.Find("HealthBarSprite").GetComponent <Image> ().sprite = Resources.Load<Sprite>("Sprites/playerHealth0_Sprite");
			break;
		default:
			Debug.Log("Error in playerHealth value: " + playerHealth);
			break;
		}

		switch (keysDropped)
		{
		case 7:
			GameObject.Find("KeysPlaced").GetComponent <Image> ().sprite = Resources.Load<Sprite>("Sprites/keysPlaced7_Sprite");
			break;
		case 6:
			GameObject.Find("KeysPlaced").GetComponent <Image> ().sprite = Resources.Load<Sprite>("Sprites/keysPlaced6_Sprite");
			break;
		case 5:
			GameObject.Find("KeysPlaced").GetComponent <Image> ().sprite = Resources.Load<Sprite>("Sprites/keysPlaced5_Sprite");
			break;
		case 4:
			GameObject.Find("KeysPlaced").GetComponent <Image> ().sprite = Resources.Load<Sprite>("Sprites/keysPlaced4_Sprite");
			break;
		case 3:
			GameObject.Find("KeysPlaced").GetComponent <Image> ().sprite = Resources.Load<Sprite>("Sprites/keysPlaced3_Sprite");
			break;
		case 2:
			GameObject.Find("KeysPlaced").GetComponent <Image> ().sprite = Resources.Load<Sprite>("Sprites/keysPlaced2_Sprite");
			break;
		case 1:
			GameObject.Find("KeysPlaced").GetComponent <Image> ().sprite = Resources.Load<Sprite>("Sprites/keysPlaced1_Sprite");
			break;
		case 0:
			GameObject.Find("KeysPlaced").GetComponent <Image> ().sprite = Resources.Load<Sprite>("Sprites/keysPlaced0_Sprite");
			break;
		default:
			Debug.Log("Error in keysPlaced value: " + keysPlaced);
			break;
		}

	}

}
