﻿// Reptilian - GAME 120 and 121 Spring 2016
// SingletonAudioSource
// Used to control background music
// Can be used to call the PlayAudio function with SingletonAudioSource.Instance.PlayAudio(SingletonSudioSource.Instance.soundClip)
// Script by Llane

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SingletonAudioSource : MonoBehaviour {

	// Public variable, set in the inspector so you can access the audio clip
	// through the singleton instance
	// public AudioClip soundClip;

	// Buttons
	public AudioClip buttonSelect;
	public AudioSource buttonSource;

	// Background music
	public AudioSource backgroundSource;

	// Sounds every level
	public AudioSource enemyNoiseSource;
	public AudioSource playerNoiseSource;
	public AudioSource keyFragmentDropSource;
	public AudioSource mindControlRaySource;

    // Static singleton variable
    public static SingletonAudioSource Instance { get; private set; }

    
	public void Awake()
    {
        // Check if there are any other instances conflicting with this instance
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }

        // Save a reference to the SingletonAudioSource component as the singleton instance
        Instance = this;

        // Make sure this object is not destroyed between scenes
        DontDestroyOnLoad(gameObject);


    }

	// Instance method; this method can be accessed through the singleton Instance
	// Play background music
	public void PlayBackground(AudioClip music)
	{
		backgroundSource.clip = music;
		backgroundSource.Play ();
		backgroundSource.loop = true;
	} // end PlayBackground

	// Instance method
	public void StopBackground()
	{
		backgroundSource.Stop ();
	}

	public void PlayEffect(int effect)
    {
		// Keep track of current scene
		Scene currentScene = SceneManager.GetActiveScene();

		switch (effect) 
		{ 
		case 1:		// button noise
			buttonSource.clip = buttonSelect;
			buttonSource.Play ();
			buttonSource.loop = false;
			break;

		case 2:			// mind control ray noise
			mindControlRaySource.clip = Resources.Load<AudioClip>("Audio/mind_control_ray_sound");
			mindControlRaySource.Play();
			mindControlRaySource.loop = false;
			break;

		case 3:			// enemy attach noise

			if(SingletonAudioSource.Instance != null )
			{
				if (currentScene.name == "10_ForestWorld")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/monkey_native_sound");
				else if (currentScene.name == "10_ForestWorldBoss")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/monkey_king_sound");
				else if (currentScene.name == "10_UrbanWorld")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/default_placeholder");
				else if (currentScene.name == "10_UrbanWorldBoss")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/default_placeholder");
				else if (currentScene.name == "10_IceWorld")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/default_placeholder");
				else if (currentScene.name == "10_IceWorldBoss")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/default_placeholder");
				else if (currentScene.name == "10_VolcanoWorld")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/default_placeholder");
				else if (currentScene.name == "10_VolcanoWorldBoss")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/default_placeholder");

				enemyNoiseSource.Play();
				enemyNoiseSource.loop = false;

			}
			break;

		case 4:			// key fragment drop
			keyFragmentDropSource.clip = Resources.Load<AudioClip>("Audio/PlaceKey");
			keyFragmentDropSource.Play();
			keyFragmentDropSource.loop = false;
			break;

		case 5:			// death noise
			if(SingletonAudioSource.Instance != null )
			{
				if (currentScene.name == "10_ForestWorld")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/default_placeholder");
				else if (currentScene.name == "10_ForestWorldBoss")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/default_placeholder");
				else if (currentScene.name == "10_UrbanWorld")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/default_placeholder");
				else if (currentScene.name == "10_UrbanWorldBoss")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("default_placeholder");
				else if (currentScene.name == "10_IceWorld")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/default_placeholder");
				else if (currentScene.name == "10_IceWorldBoss")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/default_placeholder");
				else if (currentScene.name == "10_VolcanoWorld")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/default_placeholder");
				else if (currentScene.name == "10_VolcanoWorldBoss")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/default_placeholder");

				enemyNoiseSource.Play();
				enemyNoiseSource.loop = false;
			}
			break;

		case 6: 		// subjugation noise
			if(SingletonAudioSource.Instance != null )
			{
				enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/default_placeholder");
				enemyNoiseSource.Play();
				enemyNoiseSource.loop = false;
			}
			break;

		case 7: 		// enemy hurt
			if(SingletonAudioSource.Instance != null )
			{
				if (currentScene.name == "10_ForestWorld")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/default_placeholder");
				else if (currentScene.name == "10_ForestWorldBoss")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/default_placeholder");
				else if (currentScene.name == "10_UrbanWorld")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/default_placeholder");
				else if (currentScene.name == "10_UrbanWorldBoss")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/default_placeholder");
				else if (currentScene.name == "10_IceWorld")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/default_placeholder");
				else if (currentScene.name == "10_IceWorldBoss")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/default_placeholder");
				else if (currentScene.name == "10_VolcanoWorld")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/default_placeholder");
				else if (currentScene.name == "10_VolcanoWorldBoss")
					enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/default_placeholder");

				enemyNoiseSource.Play();
				enemyNoiseSource.loop = false;
			}
			break;

		case 8:			// Smack Monkey noise
			if(SingletonAudioSource.Instance != null )
			{
				enemyNoiseSource.clip = Resources.Load<AudioClip>("Audio/Smack");
				enemyNoiseSource.Play();
				enemyNoiseSource.loop = false;
			}
			break;
				
		default:
			break;
		}


    } // end PlayEffect
}
