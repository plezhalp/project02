﻿// Reptilian - GAME 120 and 121 Spring 2016
// LevelManager --  One LevelManager script should be applied to each gameplay scene
// Used to manage information held in each level:  
//      player lives, player health, number of placeables put down, number of villagers mind controlled
// 
// Tags needed: 
// 	1. Villagers to be mind-controlled should be tagged "Villager"
//  2. Player character should be tagged "Player"
//  3. Objects that are placeholders for the respawn points should  be tagged "Respawn"

// Script by LLane

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	// HUD variables
	private int playerLives;			// 5 lives (red dot)
	private int playerHealth;			// Health bar (green rod with three sections
	private int keysDropped;			// Keys placed (7) stars that are marked off as they are placed
	private int villagerCount;			// Counter of Villagers Controlled
	private int villagersAvailable;			//Total Villagers in scene

	// Player and level variables
	private Vector3 respawnPoint;		// Location to respawn in level
	private GameObject player;			// identify player
	public int levelNumber;				// identify which level by number

	public AudioClip backgroundMusic;	// Set up background music

	// Use this for initialization
	void Start () {

		// Setup defaults
		playerLives = 5;
		playerHealth = 3;
		keysDropped = 0;
		villagerCount = 0;

		// Keep track of the current scene
		Scene currentScene = SceneManager.GetActiveScene();

		Debug.Log("This scene is" + currentScene.name);

		// Play appropriate background music for each level
		if(SingletonAudioSource.Instance != null )
		{	
			if (currentScene.name == "00_StartScene" || currentScene.name == "01_Splash_Scene" || currentScene.name == "02_Credits" || 
				currentScene.name == "03_MainMenu" || currentScene.name == "04_Instructions" || currentScene.name == "04_WinScene" || 
				currentScene.name == "10_HubWorld" )
			{
				backgroundMusic = Resources.Load<AudioClip>("Audio/Codename Granny II");
			}
			else if (currentScene.name == "10_ForestWorld")
			{
				backgroundMusic = Resources.Load<AudioClip>("Audio/forest_level_theme");
			}
			else if (currentScene.name == "10_ForestWorldBoss")
			{
				backgroundMusic = Resources.Load<AudioClip>("Audio/forest_level_theme");
			}
			else if (currentScene.name == "10_IceWorld")
			{
				backgroundMusic = Resources.Load<AudioClip>("Audio/The Lift");
			}
			else if (currentScene.name == "10_IceWorldBoss")
			{
				backgroundMusic = Resources.Load<AudioClip>("Audio/The Lift");
			}
			else if (currentScene.name == "10_UrbanWorld")
			{
				backgroundMusic = Resources.Load<AudioClip>("Audio/UrbanWorldBackground");
			}
			else if (currentScene.name == "10_UrbanWorldBoss" )
			{
				backgroundMusic = Resources.Load<AudioClip>("Audio/UrbanWorldBackground");
			}
			else if (currentScene.name == "10_VolcanoWorld")
			{
				backgroundMusic = Resources.Load<AudioClip>("Audio/VolcanoBGM");
			}
			else if (currentScene.name == "10_VolcanoWorldBoss")
			{
				backgroundMusic = Resources.Load<AudioClip>("Audio/VolcanoBGM");
			}

			SingletonAudioSource.Instance.PlayBackground(backgroundMusic);
		}

		// Count how many villagers are in the scene
		GameObject[] villagers = GameObject.FindGameObjectsWithTag("Villager");
		villagersAvailable = villagers.Length;

//		Debug.Log("Number villagers in " + currentScene.name + " is " + villagersAvailable);
//		Debug.Log("HUDController.Instance is " + HUDController.Instance);
//		Debug.Log("playerLives is " + playerLives);
//		Debug.Log("playerHealth is " + playerHealth);
//		Debug.Log("keysDropped is " + keysDropped);
//		Debug.Log("villagerCount is " + villagerCount);

		// Update the HUD with villager count
		GameObject.Find("Canvas Health HUD").GetComponent<HUDController> ().UpdateHUD(playerLives, playerHealth, keysDropped, villagerCount, villagersAvailable);
		//HUDController.Instance.UpdateHUD();

		// Set up spawn point
		respawnPoint = GameObject.FindWithTag("Respawn").transform.position;

		// identify player
		player = GameObject.FindWithTag("Player");
	}
		


	public void LoseHealth()
	{
		// Decrease health by 1
		playerHealth--;

		// Update the HUD image
		GameObject.Find("Canvas Health HUD").GetComponent<HUDController> ().UpdateHUD(playerLives, playerHealth, keysDropped, villagerCount, villagersAvailable);
		//HUDController.Instance.UpdateHUD(playerLives, playerHealth, keysDropped, villagerCount, villagersAvailable);
		//HUDController.Instance.UpdateHUD();

		if(playerHealth <= 0)
		{
			// Wait a second to let player watch their health be zero and let it sink in that the player has died
			Invoke ("Die", 1.0f);
			//Die();
		}
	}

	public void LoseHealthPolar()
	{
		// Decrease health by 1
		playerHealth = playerHealth - 2;

		// Update the HUD image
		GameObject.Find("Canvas Health HUD").GetComponent<HUDController> ().UpdateHUD(playerLives, playerHealth, keysDropped, villagerCount, villagersAvailable);
		//HUDController.Instance.UpdateHUD(playerLives, playerHealth, keysDropped, villagerCount, villagersAvailable);
		//HUDController.Instance.UpdateHUD();

		if(playerHealth <= 0)
		{
			// Wait a second to let player watch their health be zero and let it sink in that the player has died
			Invoke ("Die", 1.0f);
			//Die();
		}
	}

	public void AddHealth ()
	{
		// Check to see if player health is low
		if (playerHealth < 3)
		{
			// If player Health is low, add 1 hp
			playerHealth++;

			// Update the HUD image
			GameObject.Find("Canvas Health HUD").GetComponent<HUDController> ().UpdateHUD(playerLives, playerHealth, keysDropped, villagerCount, villagersAvailable);
			//HUDController.Instance.UpdateHUD(playerLives, playerHealth, keysDropped, villagerCount, villagersAvailable);
			//HUDController.Instance.UpdateHUD();

		}
	}

	public void IncrVillager()
	{
		// Increase villageCount
		villagerCount++;

		// Update the HUD
		GameObject.Find("Canvas Health HUD").GetComponent<HUDController> ().UpdateHUD(playerLives, playerHealth, keysDropped, villagerCount, villagersAvailable);
		//HUDController.Instance.UpdateHUD(playerLives, playerHealth, keysDropped, villagerCount, villagersAvailable);
		//HUDController.Instance.UpdateHUD();

	}

	public void UpdateKeyDrops()
	{
		// Increase or decrease key drop count
		keysDropped++;

		// call WinLevel to check to see if we have won the level (keyDrops are 7)
		WinLevel();

		// Update the HUD
		GameObject.Find("Canvas Health HUD").GetComponent<HUDController> ().UpdateHUD(playerLives, playerHealth, keysDropped, villagerCount, villagersAvailable);

		//HUDController.Instance.UpdateHUD(playerLives, playerHealth, keysDropped, villagerCount, villagersAvailable);
		//HUDController.Instance.UpdateHUD();

	}

	private void WinLevel()				// If player has placed all 7 keys, open access to boss door
	{
		if (keysDropped >= 7)
		{
			
			SingletonGameController.Instance.CompleteLevel(levelNumber-1);
			// Open the door to the boss in the level
			Destroy(GameObject.Find( ("BossDoor")));

		}
	}

	public void Die()				// Player has died...
	{
		// Decrement playerLives by 1
		playerLives--;

		// Check if player is out of lives
		if (playerLives <= 0)
		{
			// If yes, Load last save
			SceneManager.LoadScene("10_HubWorld");
		}

		else // player has died but is not out of lives
		{
			// Teleport player to spawn point in level
			player.transform.position = respawnPoint;

			// Restore player to full health
			playerHealth = 3;
			// Update HUD
			GameObject.Find("Canvas Health HUD").GetComponent<HUDController> ().UpdateHUD(playerLives, playerHealth, keysDropped, villagerCount, villagersAvailable);
			//HUDController.Instance.UpdateHUD(playerLives, playerHealth, keysDropped, villagerCount, villagersAvailable);
			//HUDController.Instance.UpdateHUD();
		}
	}

}
