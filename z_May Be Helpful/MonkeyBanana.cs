﻿using UnityEngine;
using System.Collections;

public class MonkeyBanana : MonoBehaviour
{
	Rigidbody myBody;
	float speed = 10;

	void Start()
	{
		myBody = this.gameObject.GetComponent<Rigidbody>();
		Destroy(this.gameObject, 5f);
	}

	public void AddVel(Vector3 direction)
	{
		this.gameObject.GetComponent<Rigidbody>().AddForce(direction * speed);
		//Debug.Log("My body now! " + this.gameObject.GetComponent<Rigidbody>());
		//myBody.AddForce(direction * speed);
	}

	void OnTriggerEnter(Collider playerCol)
    {
		if(playerCol.gameObject.tag == "Player")
		{
			GameObject.Find("LevelManager").GetComponent<LevelManager>().LoseHealth();
            SingletonAudioSource.Instance.PlayEffect(3);
            Destroy(this.gameObject);
		}
        
    }
}
