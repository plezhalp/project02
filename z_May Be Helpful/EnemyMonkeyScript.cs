﻿using UnityEngine;
using System.Collections;

public class EnemyMonkeyScript : MonoBehaviour
{

    public int health;
    public float subjugationTimer = 3.0f;
    public float speed = 3;
	public int objectRotation = 90;

    public GameObject player;
    public GameObject Banana;

    MonkeyLongRange rangeAttack;
    MonkeyShortRange meleeAttack;

	public bool inFirstRange;
	public bool inSecRange;
	public bool shouldMove;

    public AudioClip bananaThrow;
    public AudioClip monkeyAttacking;
    public AudioClip monkeyAttacked;
    public AudioClip monkeySubjugation;
    public AudioClip monkeyDeath;

	bool jumpAttack, bananaAttack;

    Color brownColour;

	// Use this for initialization
	void  Start()
    {
		player = GameObject.FindGameObjectWithTag("Player");
        rangeAttack = gameObject.GetComponentInChildren<MonkeyLongRange>();
        meleeAttack = gameObject.GetComponentInChildren<MonkeyShortRange>();
        brownColour = gameObject.GetComponent<Renderer>().material.color;
    }

    // Update is called once per frame
    void Update()
	{
		//Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 7.0f);

		if(!inFirstRange && !inSecRange && shouldMove)
		{
			Wander();
		}
  
    }//end Update()



	void OnCollisionEnter(Collision col)
	{
		//if this object hits a "perimeter"
		if (col.transform.tag == "perimeter")
		{
			//rotate this object around
			transform.Rotate(0, -150, 0);
		}

		if(col.transform.CompareTag("PlayerArm"))
		{
            //Make the Enemy take damage please
            TakeDamage(1);
		}

        //if (col.transform.CompareTag(""))
        //{
        //    Subjugation();
        //}
		
	}
		

	public void Attack()
	{
		if(!jumpAttack)
		{
			jumpAttack = true;
            // do melee attack
            transform.LookAt(player.transform);
            transform.Translate(Vector3.up);
            transform.Translate(Vector3.forward);
			Invoke("CoolDown", 24.0f);
		}
	}

	public void Range()
	{
		if(!bananaAttack)
		{
			bananaAttack = true;
			Invoke("CoolDown", 4.0f);
			//Turn towards player
			transform.LookAt(player.transform);
			//Instaniate and throw bananas -- CHANGED THE INSTANTIATE POSTIION TO TRANSFORM.POSITION from VECTOR3.FORWARD
			GameObject tempBanana = Instantiate(Banana, transform.position, Quaternion.identity) as GameObject;
			tempBanana.GetComponent<MonkeyBanana>().AddVel(transform.TransformDirection(Vector3.forward));
	 	}
	}

	public void CoolDown()
	{
		jumpAttack = false;
		bananaAttack = false;
	}


    void Wander()
    {
        //Sends the monkeys forward
        transform.Translate(Vector3.forward * speed * Time.deltaTime);

        //Checks to see if an object is in the way then turns the monkeys position
        Vector3 checkingForward = transform.TransformDirection(Vector3.forward);

        if (Physics.Raycast(transform.position, checkingForward, 1))
        {
            transform.Rotate(0, objectRotation, 0);
        }
    }



    public void Subjugation()
    {
        Invoke("Reactivate", 4.0f);
       
        gameObject.GetComponent<Renderer>().material.color = Color.magenta;
        rangeAttack.canShoot = false;
        meleeAttack.canJump = false;
        bananaAttack = true;

    }

    public void Reactivate()
    {
        rangeAttack.canShoot = true;
        meleeAttack.canJump = true;
        bananaAttack = false;

        gameObject.GetComponent<Renderer>().material.color = brownColour; //new Color(163, 74, 16, 1f);
        Wander();
    }
    
    public void TakeDamage(int damageAmount)
    {
        if (damageAmount > 0)
            health -= damageAmount;
        if (health <= 0)
            Destroy(gameObject);
    }
}
