﻿﻿using UnityEngine;

// Mitchell
public class Script_PlayerMovement : MonoBehaviour
{
    public float moveSpeed;
    public GameObject player;


    // Use this for initialization
    void Start()
    {
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
    }


    //set what key are used for movement
    void Movement()
    {
        if (Input.GetKey(KeyCode.D))
        {
            gameObject.transform.Rotate(Vector3.up, 1f);
        }

        if (Input.GetKey(KeyCode.W))
        {
            gameObject.transform.Translate(moveSpeed, 0, 1);
        }

        if (Input.GetKey(KeyCode.A))
        {
            gameObject.transform.Rotate(Vector3.down, 1f);
        }
    }
}