﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Script_Buttons : MonoBehaviour
{
    public void _MainMenu()
    {
        SceneManager.LoadScene("01_Scene_MainMenu");
    }

    public void _CreditsMenu()
    {
        SceneManager.LoadScene("02_Scene_Credits");
    }

    public void _TestScene()
    {
        SceneManager.LoadScene("99_Scene_Test");
    }

    public void _Level1()
    {
        SceneManager.LoadScene("10_Scene_MansionGrounds");
    }

    public void _Level2()
    {
        SceneManager.LoadScene("11_Scene_MansionFoyer");
    }

    public void _Level3()
    {
        SceneManager.LoadScene("12_Scene_Mansion2ndFloor");
    }

    public void _Level4()
    {
        SceneManager.LoadScene("13_Scene_MansionAttic");
    }

    public void _QuitGame()
    {
        Application.Quit();
    }
}
