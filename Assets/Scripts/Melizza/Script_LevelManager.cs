﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Script_LevelManager : MonoBehaviour
{
    private int playerHealth;
    private GameObject player;
    private int starterCorb;
    private int scrappyCorb;
    private int superCorb;
    private int supremeCorb;
    private int monsterCount;
    private int monstersAvailable;

    public void Start()
    {
        //setup deafaults
        playerHealth = 10;
        starterCorb = 0;
        scrappyCorb = 0;
        superCorb = 0;
        supremeCorb = 0;
        monsterCount = 0;

        GameObject[] monsters = GameObject.FindGameObjectsWithTag("Monster");
        monstersAvailable = monsters.Length;
    }//end Start()

    public void LoseHealth()
    {
        //if called, the player loses 1 "health"
        playerHealth--;

        if (playerHealth <= 0)
            Invoke("Die", 1.0f);
    }//end LoseHealth()

    public void IncrementMonster()
    {
        monsterCount++;
    }//end IncrementMonster()

    private void WinLevel()
    {
        if (monstersAvailable == 0)
        {
            //end game with game controller
        }
    }//end WinLevel()

    public void Die()
    {
        //put in a death scene
        SceneManager.LoadScene("03_SceneDeath");
    }//end Die()

}
