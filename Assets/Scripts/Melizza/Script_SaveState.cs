﻿using UnityEngine;
using System.Collections;

public class Script_SaveState : MonoBehaviour
{
    public static Script_SaveState Instance { get; private set; }

    public bool[] levelProgress = new bool[4];

    public bool CLEAR_ALL_SAVES = false;

   public void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public void Start()
    {
        for (int i = 0; i < levelProgress.Length; i++)
        {
            levelProgress[i] = false;
        }

        if (CLEAR_ALL_SAVES)
        {
            for (int i = 0; i < levelProgress.Length; i++)
            {
                PlayerPrefs.DeleteKey(("Level " + i));
            }

            CLEAR_ALL_SAVES = false;
        }

        Load();
    }

    public void Load()
    {
        if (PlayerPrefs.HasKey("Level 0"))
        {
            for (int i = 0; i < levelProgress.Length; i++)
            {
                levelProgress[i] = PlayerPrefsIntToBool(PlayerPrefs.GetInt(("Level " + i)));
            }
        }
    }

    public void Save()
    {
        for (int i = 0; i < levelProgress.Length; i++)
        {
            PlayerPrefs.SetInt(("Level " + i), PlayerPrefsBoolToInt(levelProgress[i]));
        }
    }

    private int PlayerPrefsBoolToInt(bool boolToConvert)
    {
        if (boolToConvert)
            return 1;
        else
            return 0;
    }

    private bool PlayerPrefsIntToBool(int intToConvert)
    {
        if (intToConvert == 1)
            return true;
        else
            return false;
    }

    public void CompleteLevel(int levelNum)
    {
        levelProgress[levelNum] = true;
        Save();
    }
}
