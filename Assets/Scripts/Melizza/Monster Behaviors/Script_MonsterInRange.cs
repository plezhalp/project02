﻿using UnityEngine;
using System.Collections;

public class Script_MonsterInRange : MonoBehaviour
{
    Script_MonsterWander wanderScript;
    public bool withinRange = true;

	// Use this for initialization
	void Start ()
    {
        wanderScript = GetComponentInParent<Script_MonsterWander>();
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    void OnTriggerStay(Collider other)
    {
        if (other.transform.CompareTag("Player"))
        {
            wanderScript.inRange = true;
            if (withinRange == true)
            {
                //withinRange = false;
                Invoke("CoolDown", 3.0f);
                wanderScript.Attack();
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.transform.CompareTag("Player"))
        {
            wanderScript.inRange = false;
        }
    }

    void CoolDown()
    {
        withinRange = true;
    }
}
