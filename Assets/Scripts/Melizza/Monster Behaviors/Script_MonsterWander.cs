﻿using UnityEngine;
using System.Collections;

public class Script_MonsterWander : MonoBehaviour
{
    public bool caught = false;
   // public bool shouldMove = false;
    public bool attacking = false;
    public bool inRange;
    public float speed = 3;
    public float objectRotation = 90;

    public GameObject player;

    Script_MonsterInRange checkInRange;

	// Use this for initialization
	void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!inRange)
        {
            Wander();
        }
    }

    void OnCollisionEnter(Collision col)
    {
        //if this object hits a "perimeter"
        if (col.transform.tag == "perimeter")
        {
            //rotate this object around
            transform.Rotate(0, -150, 0);
        }

        if (col.transform.CompareTag("CorbStarter"))
        {
            //Make the Enemy die
            Destroy(gameObject);
        }
    }

    void Wander()
    {
        //Sends the monsters forward
        transform.Translate(Vector3.left * speed * Time.deltaTime);

        //Checks to see if an object is in the way then turns the monsters position
        Vector3 checkingForward = transform.TransformDirection(Vector3.forward);

        if (Physics.Raycast(transform.position, checkingForward, 1))
        {
            transform.Rotate(0, objectRotation, 0);
        }
    }

    public void Attack()
    {
        if(!attacking)
        {
            attacking = true;
            Invoke("CoolDown", 4.0f);
            transform.LookAt(player.transform);
        }
    }

    public void CoolDown()
    {
        attacking = false;
    }
}
