﻿using UnityEngine;
using System.Collections;

public class Script_CorbSupreme : MonoBehaviour
{
    Rigidbody myBody;
    float speed = 10;

    void Start()
    {
        myBody = this.gameObject.GetComponent<Rigidbody>();
        Destroy(this.gameObject, 6.0f);
    }

    public void AddVelocity(Vector3 direction)
    {
        this.gameObject.GetComponent<Rigidbody>().AddForce(direction * speed);
    }

    void OnTriggerEnter(Collider monsterCol)
    {
        if (monsterCol.gameObject.tag == "Monster1")
        {
            //GameObject.Find("LevelManager").GetComponent<Script_LevelManager>().IncrementMonster;
            Script_SingletonAudioSource.Instance.PlayEffect(1);
            Destroy(this.gameObject);
        }

        if (monsterCol.gameObject.tag == "Monster2")
        {
            //GameObject.Find("LevelManager").GetComponent<Script_LevelManager>().IncrementMonster;
            Script_SingletonAudioSource.Instance.PlayEffect(1);
            Destroy(this.gameObject);
        }

        if (monsterCol.gameObject.tag == "Monster3")
        {
            //GameObject.Find("LevelManager").GetComponent<Script_LevelManager>().IncrementMonster;
            Script_SingletonAudioSource.Instance.PlayEffect(1);
            Destroy(this.gameObject);
        }

        if (monsterCol.gameObject.tag == "Monster4")
        {
            //GameObject.Find("LevelManager").GetComponent<Script_LevelManager>().IncrementMonster;
            Script_SingletonAudioSource.Instance.PlayEffect(1);
            Destroy(this.gameObject);
        }
    }

}
