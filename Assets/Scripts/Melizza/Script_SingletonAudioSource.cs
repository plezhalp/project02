﻿using UnityEngine;
using System.Collections;

public class Script_SingletonAudioSource : MonoBehaviour
{
    
    //background music
    public AudioSource backgroundSource;

    //sounds every lever
    public AudioSource effectsSource;

    //sound effects every level
    public AudioClip monsterCaught;
    public AudioClip monsterEscaped;
    public AudioClip playerDying;
    public AudioClip monInRange;

    public static Script_SingletonAudioSource Instance{ get; private set;}

    public void Awake()
    {
        //checks to see if any other instances are conflicting with this instance
        if (Instance != null && Instance != this)
            Destroy(gameObject);

        //save reference to the script component as the singleton instance
        Instance = this;

        //make sure this object is not destroyed between scenes
        DontDestroyOnLoad(gameObject);
    }//end Awake()

    public void PlayBackground(AudioClip music)
    {
        backgroundSource.clip = music;
        backgroundSource.Play();
        backgroundSource.loop = true;
    }//end PlayBackground()

    public void StopBackground()
    {
        backgroundSource.Stop();
    }//end StopBackground()

    public void PlayEffect(int effect)
    {
        //keeping track of our current scene
        //Scene currentScene = SceneManager.GetActiveScene();

        switch (effect)
        {
            case 1:
                effectsSource.clip = monsterCaught;
                effectsSource.Play();
                effectsSource.loop = false;
                break;
            case 2:
                effectsSource.clip = monsterEscaped;
                effectsSource.Play();
                effectsSource.loop = false;
                break;
            case 3:
                effectsSource.clip = playerDying;
                effectsSource.Play();
                effectsSource.loop = false;
                break;
            case 4:
                effectsSource.clip = monInRange;
                effectsSource.Play();
                effectsSource.loop = true;
                break;
        }
    }//end PlayEffect()
}
